import React, { Component } from 'react';
class VideoCapture extends React.Component {
    constructor(props) {
      super(props);
      this.streamCamVideo= this.streamCamVideo.bind(this)
      this.takepicture= this.takepicture.bind(this)
      this.state = {
        constraints: { audio: false, video: { width: 128, height: 128 } }
      };
    }

    componentDidMount(){
        this.streamCamVideo();
    }

    componentWillUnmount() {
      this.stopStreamedVideo();
    }

      takepicture() {
        const canvas = document.querySelector('canvas');
        const context = canvas.getContext('2d');
        const video = document.querySelector('video');
        const photo = document.getElementById('photo');
        const { width, height } = this.state.constraints.video;

        canvas.width = width;
        canvas.height = height;
        context.drawImage(video, 0, 0, width, height);

        const data = canvas.toDataURL('image/png');
        photo.setAttribute('src', data);
      }

      
    
    stopStreamedVideo() {
      const stream = document.querySelector("#videoCapture").srcObject;
      const tracks = stream.getTracks();
    
      tracks.forEach(function(track) {
        track.stop();
      });
    
      document.querySelector("#videoCapture").srcObject = null;
    }

    streamCamVideo() {
      var constraints = { audio: false, video: { width: 128, height: 128 } };
      navigator.mediaDevices
        .getUserMedia(constraints)
        .then(function(mediaStream) {
          var video = document.querySelector("#videoCapture");
          video.srcObject = mediaStream;
          video.onloadedmetadata = function(e) {
            video.play();
          };
        })
        .catch(function(err) {
          console.log(err.name + ": " + err.message);
        }); 
    }
    render() {
      return (
        <div>
          <div id="container">
            <video autoPlay={true} id="videoCapture" controls></video>
          </div>
          <br/>
          {/* <button onClick={this.takepicture}>Start streaming</button> */}
          <canvas id="canvas"></canvas>
           <img id="photo" alt="The screen capture will appear in this box."/> 
        </div>
      );
    }
}
    export default VideoCapture;